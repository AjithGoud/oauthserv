package com.khatri.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.khatri.model.Roles;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer>
{
	public Roles getByRole(String role);
}
