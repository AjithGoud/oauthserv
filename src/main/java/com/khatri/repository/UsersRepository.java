package com.khatri.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.khatri.model.UsersEntity;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {

	public UsersEntity getByEmailId(String emailId);

	public UsersEntity getUserByEmailId(String emailId);

	public UsersEntity getByIsActive(boolean isActive);

}
